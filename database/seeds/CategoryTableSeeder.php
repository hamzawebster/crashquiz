<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category')->insert([
            'name' => "Sports",
            'description' => '',
            'status' => 'active',
        ]);
        
        DB::table('category')->insert([
            'name' => "Education",
            'description' => '',
            'status' => 'active',
        ]);

        DB::table('category')->insert([
            'name' => "Entertainment",
            'description' => '',
            'status' => 'active',
        ]);
    }
}
