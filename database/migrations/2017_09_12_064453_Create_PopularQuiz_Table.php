<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePopularQuizTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    
    public function up()
    {
        Schema::create('PopularQuiz', function (Blueprint $table) {
           
            $table->increments('id');
            
            $table->integer('Quiz_id')->nullable();
             $table->integer('category_id')->nullable();
			
            $table->integer('uid')->nullable();
            $table->timestamps();
             });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
                       
    public function down()
    {
        Schema::dropIfExists('PopularQuiz');
    }
}
