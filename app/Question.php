<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
	protected $table = 'question';
    /**
     * Get the Quiz record associated with the Question.
     */
    public function quiz()
    {
        return $this->hasOne('App\Quiz');
    }
}
