<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Events\User\PermissionCreated;
use App\Events\User\RoleCreated;
use App\Http\Requests\EditRoleRequest;
use App\Http\Requests\SavePermissionRequest;
use App\Http\Requests\SaveRoleRequest;
use App\Http\Requests\SettingAddRequest;
use App\Services\User\UserImport;
use App\TempTable;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Setting;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Routing\Redirector;
use Session;
class QuestionController extends Controller
{
    /**
     * Update the specified user.
     *
     * @param  Request  $request
     * @param  string  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        
    }
	
     public function handleResults(Request $request)
    {
        	$q=$request->quiz_id;
    $correct=$request->correctOpt;
		 
		 
		 if($request->QuestionCount < 1)
		 {
			 $executed = DB::table('quiz')
			->where('id', $request['quiz_id'])
	            ->update([
	            	'score_count' => 0,
					
				]);
		 }
	//dd($request->category_id);
	
	/* $c= input::has('r1') ? true:false;
	if($c==True)
	{
		dd($request->r1);
	} */
	
///Option 1
	
if($request->opt1 != null)
	{
			DB::table('quiz')->where('id', $request->quiz_id)->increment('QuestionCount');
			if($request->QuestionCount < 6)
	{
	  		$Opt1=$request->opt1;
		if ($Opt1 == $correct)
	{
			DB::table('quiz')->where('id', $request->quiz_id)->increment('score_count',5);
			
			 
			
			
		 
		return redirect()->route('user.questions',$q);
	}
	else
	{
		
          return redirect()->route('user.questions',$q);	
	}
	}
			else
	{
		$executed = DB::table('quiz')
			->where('id', $request['quiz_id'])
	            ->update([
	            	'QuestionCount' => 0,
					
				]);
				
				Session::flash('sscore','this is msg');
				
	 return redirect()->route('user.result',$q);
	}
	 
	}
	
  
//Option2
	
	
		if($request->opt2 != null)
	{
			DB::table('quiz')->where('id', $request->quiz_id)->increment('QuestionCount');
			if($request->QuestionCount < 6)
	{
	  		$Opt2=$request->opt2;
		if ($Opt2 == $correct)
	{
			DB::table('quiz')->where('id', $request->quiz_id)->increment('score_count',5);
			
			 
			
			
		 
		return redirect()->route('user.questions',$q);
	}
	else
	{
		
          return redirect()->route('user.questions',$q);	
	}
	}
			else
	{
		$executed = DB::table('quiz')
			->where('id', $request['quiz_id'])
	            ->update([
	            	'QuestionCount' => 0,
					
				]);
				
				Session::flash('sscore','this is msg');
				
	 return redirect()->route('user.result',$q);
	}
	 
	}
	
	
//Option3
	
		if($request->opt3 != null)
	{
			DB::table('quiz')->where('id', $request->quiz_id)->increment('QuestionCount');
			if($request->QuestionCount < 6)
	{
	  		$Opt3=$request->opt3;
		if ($Opt3 == $correct)
	{
			DB::table('quiz')->where('id', $request->quiz_id)->increment('score_count',5);
			
			 
			
			
		 
		return redirect()->route('user.questions',$q);
	}
	else
	{
		
          return redirect()->route('user.questions',$q);	
	}
	}
			else
	{
		$executed = DB::table('quiz')
			->where('id', $request['quiz_id'])
	            ->update([
	            	'QuestionCount' => 0,
					
				]);
				
				Session::flash('sscore','this is msg');
				
	 return redirect()->route('user.result',$q);
	}
	 
	}
	
//Option4
		
		if($request->opt4 != null)
	{
			DB::table('quiz')->where('id', $request->quiz_id)->increment('QuestionCount');
			if($request->QuestionCount < 6)
	{
	  		$Opt4=$request->opt4;
		if ($Opt4 == $correct)
	{
			DB::table('quiz')->where('id', $request->quiz_id)->increment('score_count',5);
			
			 
			
			
		 
		return redirect()->route('user.questions',$q);
	}
	else
	{
		
          return redirect()->route('user.questions',$q);	
	}
	}
			else
	{
		$executed = DB::table('quiz')
			->where('id', $request['quiz_id'])
	            ->update([
	            	'QuestionCount' => 0,
					
				]);
				
				Session::flash('sscore','this is msg');
				
	 return redirect()->route('user.result',$q);
	}
	 
	}
	}
   
    public function handleimportquestions(Request $request)
    {
        
      //print_r($request);
      $this->validate($request,[
        'file' => 'required|mimes:csv,txt'
      ]);
        
        
        
        if(($handle = fopen($_FILES['file']['tmp_name'],"r")) !== FALSE){
        fgetcsv($handle); // remove first row of excel file such as id,title,description
        while(($data = fgetcsv($handle,1000,",")) !==FALSE){
        $addPro =  DB::table('question')->insert([
            //'id' => $data[0],
            'title' => $data[0],
            'description' => $data[1],
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
            'status' => '0',
            'option1' => $data[5],
            'option2' => $data[6],
            'option3' => $data[7],
            'option4' => $data[8],
            'quiz_id' => $data[9],
           ]);
            
        }
            
      }
        return Response::json(array(
            'error' => false,
            'status' => 'success',
            'status_code' => 200
        
        ));
        
        
    }
    
}
                                         
                                                
    
                                                

    

