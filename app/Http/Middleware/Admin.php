<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Auth;
class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		
		/*$user=User::where('usertype','admin');*/
		 $x= auth()->guard()->getUser()->usertype;
		
		if($x=="admin"){
			return $next($request);	
		}else{
			return redirect()->route('user.home');			
		}
        
    }
}
