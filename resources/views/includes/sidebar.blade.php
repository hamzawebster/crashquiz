<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->name }}</p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- search form (Optional) -->
      <form action="{{ route('admin.Searchquizzes') }}" method="get" class="sidebar-form">
          
        <div class="input-group">
          <input type="text" name="q" id="q" class="form-control" placeholder="Search Quiz|Categoroy">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
        
        
        
        
        
        
        
        
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <li class="header">Content Management</li>
        <!-- Optionally, you can add icons to the links -->
        <li class="active"><a href="{{ route('admin.quizzes') }}"><i class="fa fa-link"></i> <span>Quizzes</span></a></li>
        <li class="active"><a href="{{ route('admin.categories') }}"><i class="fa fa-link"></i> <span>Categories</span></a></li>
          <li class="active"><a href="{{ route('admin.featured') }}"><i class="fa fa-link"></i> <span>Featured Quizzes</span></a></li>
		   <li class="active"><a href="{{ route('admin.manageusers') }}"><i class="fa fa-link"></i> <span>Manage Users</span></a></li>
        <!-- <li><a href="#"><i class="fa fa-link"></i> <span>Questions</span></a></li> -->
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>