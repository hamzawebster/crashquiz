<meta property="og:url"content="{{env('QUIZ_LINK')}}{{$questions->first()->quiz_slug}}_{{$questions->first()->quiz_id}}" />
		<meta property="og:image" content="{{URL::asset($questions->first()->quiz_image)}}" />
		<meta property="og:image:width" content="300"/>
		<meta property="og:image:height" content="298"/>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta property="og:type" content="website" />
		<meta property="og:description" content="{{$questions->first()->quiz_description}}" />
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

