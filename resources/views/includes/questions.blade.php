<br><br><br>
		<div style="border:2px;" id="choice" class="container">
	<center>
			<h1><b>{{$questions->first()->quiz_name}}</b></h1>
			
			<img  class="box_logo" src="\{{$questions->first()->quiz_image}}"  >

		<h3><b>{{$questions->first()->quiz_description}}</b></h3>

			<!--Expert Mode Button
<a title="Play With Time delay Minus Points" style="background-color:#B7A1A1;margin-top:4px;" id="expert" class="button button-royal button-giant"><i class="fa fa-fighter-jet"></i> Expert Mode</a>-->
			

		<a 
		   title="Play Without Time delay Minus Points" style="background-color: #B7A1A1;margin-top:4px;padding-left:42px;padding-right:50px;text-decoration:none" id="easy" class="button button-royal button-giant"><i class="fa fa-bicycle fa-1x" aria-hidden="true"></i>
          Start Quiz
		</a>
			</center>
		
	</div>
@foreach($questions as $q )	
<div style="display:none;" id="q{{$loop->iteration}}" class="container">
	<h1><b>{{$questions->first()->quiz_name}}</b></h1>
	<div class="row" id="title">
		<h3 class="question_title">{{$q->title}}</h3>
	</div>
    <br>
	<div style="padding-top: 21px;height: 200px;"  class="jumbotron">
    	<button value="1" class="bht btn btn-default qoption">{{$q->option1}}</button>
    	<button value="2" class="bht btn btn-default qoption">{{$q->option2}}</button>
    	<button value="3" class="bht btn btn-default qoption">{{$q->option3}}</button>
		@if($q->option4 != null)
    		<button value="4" class="bht btn btn-default qoption">{{$q->option4}}</button>
		@endif
        <input type="hidden" id="q{{$loop->iteration}}correct" value="{{$q->correct_answer}}" class="correct_answer"/>	
	</div>
</div>
   
@endforeach

<div id="result" style="display:none;margin-top: 42px;" class="container">
	
	<div style="padding-top: 21px;height: 200px;"  class="jumbotron"> 

			<h2>{{ Auth::user()->name }} scored: <span id="inc1">0</span>/50 </h2>
			<h3>Thanks for playing </h3>
			
	</div>
			<br>
	<div>
		<a class="btn btn-lg btn-default" href="{{route('user.home')}}">Play some more exciting Quiz!! 
		</a>
	</div>

		<br>
	<!-- <div id="shareIcons">
	
	</div>  -->
	
	
	<br>
</div>
<div class="container">
<div style="display:none;" id="progress">
	<p style="color:#1B3A69;"><b>Quiz Progress <span id="pinc"> 0</span>%</b></p>
<progress  id="progressbar" class="progress is-large is-info" max="100" value="1"></progress>
	</div>
</div>

<div id="scr" style="float:left;width:100%;margin-left:35px;display:none">
	<h4>Score:</h4>
			<div style="margin-left:20px" class="circleBase type3">
			<h2 id="inc" style="margin-top:30px;text-align: center;">
			   0
				</h2>
			</div>
	</div>
<div class="pull-right" id="mip" style="float:left;margin-right:45px;display:none">
	<h4>Minus Points </h4>
			<div style="margin-left:20px" class="circleBase type3">
			<h2 id="test" style="margin-left:30px;margin-top:30px">
			   0
				</h2>
			</div>
	</div>





<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
$(document).ready(function(){
 
    var current_question = 1;
	$("#easy").click(function(){
		
        $("#choice").hide();
		$("#q1").fadeIn(2000);
		$("#scr").slideDown(2000);
		 $("#progress").fadeIn(2000);

    });
	

	$("button.qoption").click(function(){

        $('#pinc').text(function(i,oldVal ){
            return parseInt(oldVal ,10) + 10 ;
        });
       
        move();
        var correct_answer = $(this).closest('div').find('input.correct_answer').val();
        if ($(this).val() == correct_answer)
        {
            $(this).css('background-color','green');
            $('#inc').text(function(i,oldVal ){
                return parseInt(oldVal ,10) + 5 ;
            });
			 $('#inc1').text(function(i,oldVal ){
            return parseInt(oldVal ,10) + 5 ;
        });
        } else {
            	$(this).css('background-color','red');
        		}

         
        $( "#q" + current_question ).fadeOut( 300, function() {
            $("#q" + (current_question + 1)).fadeIn(300); 
            current_question++;
			
			if(current_question > $('h3.question_title').length)
				  {
					  $("#result").fadeIn(2000);
					  $("#scr").fadeOut(1000);
				  }
			
        });
              
    });
   
    var progress = document.getElementById("progressbar");
    function move() {
        var a = setInterval(function() {
            progress.value = progress.value + 1;
            if (progress.value%10 == 0){
                clearInterval(a);
            }
        },
        60);
    }
   
});
</script>

<!-- Share nd like on facebook -->

	 <script type="text/javascript">
	$(function(){
		//var linkurl='{{env('QUIZ_LINK')}}';
		
		var linkurl='{{env('QUIZ_LINK')}}{{$questions->first()->quiz_slug}}_{{$questions->first()->quiz_id}}';
		$("#shareIcons").jsSocials({
			url:linkurl,
			text:'Crash Quiz Challange your knowledge',
			showLable:false,
			showCount:"inside",
			shareIn:"popup",
			shares:["twitter","facebook","googleplus","pinterest"]
		});
		
	});

</script> 


