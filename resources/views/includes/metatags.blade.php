<meta property="og:url"content="{{env('HOME_LINK')}}" />
		<meta property="og:image" content="https://lh3.googleusercontent.com/8BhbMuvnYckjUaSCyxqtFx41xg11ISEIAZKoUDl80osp7QZi6yeSevJ_QOubcC16iZw=w300" />
		<meta property="og:image:width" content="300"/>
		<meta property="og:image:height" content="298"/>
		<meta property="og:type"content="website" />
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta property="og:type" content="website" />
		<meta property="og:description" content="How much you know?" />
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">