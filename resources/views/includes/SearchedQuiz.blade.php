

<div class="container" align="center">
	<h2 id="topheading"><b>Search Results for</b> *<b style="color:#2748A5">{{$keyword}}</b>*</h2>

@foreach ($quizzes as $q)       
         <div  class="col-sm-3" >
                    <article class="col-item">
                    	<div class="photo">
							
                            <a href="{{ route('user.questions', [$q->slug,$q->id]) }}">
                                <img src="/{{$q->image}}" class="img-responsive " id="rt" alt="{{$q->name}}" />
                            </a>
							
                		</div>
                		<div class="info">
                			<div class="row">
        						<a style="display:block;" 
								   href="{{ route('user.questions', [$q->slug,$q->id]) }}">
                    				<div class="price-details col-md-6">
                    					<h1 style="">{{ $q->name }}</h1>
                    				</div>
        						</a>
                			</div>
        					<a class="btn btn-success btn-block" href="{{ route('user.questions', [$q->slug,$q->id]) }}" ><b>Play Now</b></a>
                		</div>
                	</article>
                </div>
        	@endforeach
</div>
