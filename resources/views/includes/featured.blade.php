
<div style="back_img">
	
     <div align="center">
<h2 id="topheading"><b>Featured Quiz</b></h2>
</div>
<div class="container">
	

 @foreach ($featuredquizzes as $q)       
          <div  class="col-sm-3" >
                    <article class="col-item">
                    	<div class="photo">
							
                            <a href="{{ route('user.questions', [$q->slug,$q->Quiz_id]) }}">
                                <img src="/{{$q->image}}" class="img-responsive " id="rt" alt="{{$q->quiz_name}}" />
                            </a>
							
                		</div>
                		<div class="info">
                			<div class="row">
        						<a style="display:block;" 
								   href="{{ route('user.questions', [$q->slug,$q->Quiz_id]) }}">
                    				<div class="price-details col-md-6">
                    					<h1 style="">{{ $q->quiz_name }}</h1>
                    				</div>
        						</a>
                			</div>
        					<a class="btn btn-success btn-block" href="{{ route('user.questions', [$q->slug,$q->Quiz_id]) }}" ><b>Play Now</b></a>
                		</div>
                	</article>
                </div>
        	@endforeach
        
	
</div>


</div>




