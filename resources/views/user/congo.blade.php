<html>
<head>
	@include('includes.metatags')
 
  <title>Crash Quiz</title>
  
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- datatable search css-->
      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="/css/questions.css">

  
  <link rel="stylesheet" href="/css/navbar.css">
<link rel="shortcut icon" href="{{ asset('assets/quiz/favicon.ico') }}">
  


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
	@include('includes.GoogleAnalytics')
</head>
<body>

        	<div id="back_img">	
				<center>
				<div class="container">
	
	<h2 style="#eee">
	<b>Welcome to the Crash Quiz</b>
	</h2>
	<img src="https://lh3.googleusercontent.com/8BhbMuvnYckjUaSCyxqtFx41xg11ISEIAZKoUDl80osp7QZi6yeSevJ_QOubcC16iZw=w300">
				</div>
					<form method="get" action="{{route('login.fb')}}" >
		<input type="submit" class="btn btn-lg btn-success" value="Play some exciting Quiz"/>
		</form>
				</center>

    
	</div>
      @include('includes.userfooter')

    @include('includes.requiredjs')
</body>

</html>
