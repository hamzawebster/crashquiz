<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#">
	<head>
		@include('includes.metatags')
  <title>Crash Quiz</title>
 
  <!-- Bootstrap 3.3.6 -->
	
  <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- datatable search css-->
      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">

  
  	<link rel="stylesheet" href="/css/navbar.css">
    <link rel="stylesheet" href="/css/bigSearch.css">
    <link rel="stylesheet" href="/css/questions.css">
    <link rel="stylesheet" href="/css/PopularQuiz.css">
	<link rel="shortcut icon" href="{{ asset('assets/quiz/favicon.ico') }}">
	
	@include('includes.GoogleAnalytics')

</head>
	
<body>
<!-- Trending page is also the same -->
		@include('includes.usernavbar')
	<div id="back_img">
			@include('includes.userBigSearch')
			@include('includes.PopularQuiz')
	</div>
		

			@include('includes.userfooter')
	
			@include('includes.requiredjs')
	


</body>

</html>
