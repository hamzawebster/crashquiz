@extends('layouts.adminlisting')

@section('page_title', 'Quizzes')

@section('page_description', 'Quizzes added to the system')

@section('content')
    <div class="alert alert-success fade" style="display: none">
      <strong>Success!</strong> Your question has been saved
    </div>
    <div class="alert alert-danger fade" style="display: none">
      <strong>Danger!</strong> Indicates a dangerous or potentially negative action.
    </div>
    <div style="width: 15%; margin-bottom: 10px">
        <button type="button" id="newquiz" class="btn btn-block btn-success" data-toggle="modal" data-target="#exampleModal">New Quiz</button>
    </div>
    <table id="example" class="table table-striped table-bordered datatable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th width="50%">Description</th>
                <th>Category</th>
                <th>Category ID</th>
                <th>Status</th>
                <th>Added On</th>
                <th>Options</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Description</th>
                <th>Category</th>
                <th>Category ID</th>
                <th>Status</th>
                <th>Added On</th>
                <th>Options</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach ($quizzes as $quiz)
            <tr>
                <td>{{ $quiz->id }}</td>
                <td>{{ $quiz->name }}</td>
                <td>{{ $quiz->description }}</td>
                <td>{{ $quiz->category_name }}</td>
                <td>{{ $quiz->category_id }}</td>
                <td>{{ $quiz->status }}</td>
                <td>{{ $quiz->created_at }}</td>
                <td><a class="edit_button" href="#" data-toggle="modal" data-target="#exampleModal">Edit</a>
				 <a href="{{ route('admin.questions', ['id' => $quiz->id]) }}" >Questions</a>	|  <a href="{{ route('delete.quiz', ['id' => $quiz->id]) }}" >Delete</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Edit Quiz</h4>
          </div>
          <div class="modal-body">
            <form id="editquiz">
              {{ csrf_field() }}
              <input type="hidden" id="id" name="id">
              <div class="form-group">
                <label for="name" class="control-label">Quiz Name:</label>
                <input type="text" class="form-control" id="name" name="name" required="">
              </div>
              <div class="form-group">
                <label for="description" class="control-label">Description:</label>
                <textarea class="form-control" id="description" name="description"></textarea>
              </div>
              <div class="form-group">
                <label for="status" class="control-label">Category:</label>
                <select class="form-control" id="category" name="category">
                    <option value="">Select a Category</option>
                  @foreach ($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name}}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label for="status" class="control-label">Status:</label>
                <select class="form-control" id="status" name="status">
                  <option value="active">Active</option>
                  <option value="inactive">Inactive</option>
                </select>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="save" data-dismiss="modal">Save Quiz</button>
          </div>
        </div>
      </div>
    </div>
@endsection
@section('page-js-script')
<script type="text/javascript">
$(document).ready(function() {
    var table = $('.datatable').DataTable();
    
    $('#newquiz').click(function() {
        $('#id').val('');
        $('#name').val('');
        $('#description').val('');
        $('#category').val('');
        $('#status').val('inactive');
        $('#exampleModalLabel').val('New Quiz');
    });
    
    $('.datatable tbody').on( 'click', 'a.edit_button', function () {
        var question = table.row( $(this).parents('tr') ).data();
        console.log( question );

        //$('.dropdown-toggle').dropdown()
        $('#id').val(question[0]);
        $('#name').val(question[1]);
        $('#description').val(question[2]);
        $('#category').val(question[4]);
        $('#status').val(question[5]);
    });

    $('#save').click(function(){
      // Use Ajax to submit form data
      var editOrNew = $('#exampleModalLabel').val();
      var url = '/admin/quiz/save';
      if(editOrNew === 'New Quiz') {
          url = '/admin/quiz/new';
      }
      $.ajax({
          url: url,
          type: 'POST',
          data: $('#editquiz').serialize(),
          success: function(result) {
              // ... Process the result ...
              if(result.status == 'success')
              {
                $('.alert-success').show();
                $('.alert-success').addClass("in");
                window.setTimeout(function () {
                    $('.alert-success').hide();
                }, 1000);

              } else {
                $('.alert-danger').show();
                $('.alert-danger').addClass("in");
                window.setTimeout(function () {
                    $('.alert-danger').hide();
                }, 1000);
              }
              console.log(result);
          }
      });
        
    })

});
</script>


@endsection
<script src="/js/parsley.min.js"></script>