<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\config\session;

Route::get('/login', function (){
	return view('Auth/login')->name('user.login');
});    

//userHome
Route::get('/',function(){
	$quizzes1=DB::Table('quiz')    
    
   ->select('id','name','slug','category_id','image','view_count')  
    ->orderBy('view_count','DESC')
    ->take(8)    
	->get();
    return view('user/userHome',['quizzes1'=>$quizzes1]);
})->name('user.home');


Route::get('/admin', function () {
	$quizzes1=DB::Table('quiz')    
    
   ->select('id','name','image','slug','category_id','thumbnail','view_count')  
    ->orderBy('view_count','DESC')
    ->take(8)    
	->get();
	
    return view('admin',['quizzes1'=>$quizzes1]);
})->middleware('auth','admin');

Route::get('/admin/quizzes', function () {
	$quizzes = DB::table('quiz')
                ->leftjoin('category', 'quiz.category_id', '=', 'category.id')
                ->select('quiz.*', 'category.name As category_name')
                ->get();
    $categories = DB::table('category')->select('id', 'name')->get();

    return view('admin/quizlisting', ['quizzes' => $quizzes, 'categories' => $categories]);
})->middleware(['auth','admin'])->name('admin.quizzes');


//get featured for admin
Route::get('/admin/featured', function () {
	$featuredquizzes = DB::table('featuredquizzes')
                ->leftjoin('quiz', 'featuredquizzes.Quiz_id', '=', 'quiz.id')
                ->select('featuredquizzes.*', 'quiz.name As quiz_name','quiz.description As quiz_description')
                ->get();
    $quizzes = DB::table('quiz')->select('id', 'name','description')->get();

    return view('admin/featuredQuiz', ['featuredquizzes' => $featuredquizzes, 'quizzes' => $quizzes]);
})->middleware('auth','admin')->name('admin.featured');

//get featured for user
Route::get('/featured-quizzes', function () {
	$featuredquizzes = DB::table('featuredquizzes')
                ->leftjoin('quiz', 'featuredquizzes.Quiz_id', '=', 'quiz.id')
                ->select('featuredquizzes.*', 'quiz.name As quiz_name', 'quiz.slug', 'quiz.description As quiz_description','quiz.image as image','quiz.id As quiz_id')
                ->get();
    $quizzes = DB::table('quiz')->select('id', 'name','thumbnail')->get();

    return view('user/userFeatured', ['featuredquizzes' => $featuredquizzes, 'quizzes' => $quizzes]);
})->middleware('auth')->name('user.featured');





//quiz Adminsearch result
Route::get('/admin/Searchquizzes',function (Request $request) {
    //dd($request->q);

    $quizzes = DB::table('quiz')
                ->leftjoin('category', 'quiz.category_id', '=', 'category.id')
                ->where('quiz.name','LIKE','%'.($request->q).'%')
                ->orwhere('category.name','LIKE','%'.($request->q).'%')
                ->orwhere('quiz.id','=',($request->q))
                ->orwhere('category.id','LIKE','%'.($request->q).'%')
                ->select('quiz.*', 'category.name As category_name')
                ->get();

    $categories = DB::table('category')->select('id', 'name')->get();

    return view('admin/quizlisting', ['quizzes' => $quizzes, 'categories' => $categories]);

})->middleware('auth','admin')->name('admin.Searchquizzes');
///user Search
Route::get('/search',function (Request $request) {
    

    $quizzes = DB::table('quiz')
                ->leftjoin('category', 'quiz.category_id', '=', 'category.id')
                ->where('quiz.status','=','active')
                ->where('quiz.name','LIKE','%'.($request->q).'%')
                ->orwhere('category.name','LIKE','%'.($request->q).'%')
                ->orwhere('category.id','LIKE','%'.($request->q).'%')
                ->select('quiz.*', 'category.name As category_name')
                ->get();

    $categories = DB::table('category')->select('id', 'name')->get();
$keyword=$request->q;
    return view('user/searchresult', ['keyword'=>$keyword,'quizzes' => $quizzes, 'categories' => $categories]);

})->name('user.Searchquizzes');

///get categories admin
Route::get('/user/categories', function () {
	$categories = DB::table('category')->get();

    return view('admin/categorylisting', ['categories' => $categories]);
})->middleware('auth','admin')->name('user.category');


///get categories admin
Route::get('/admin/categories', function () {
	$categories = DB::table('category')->get();

    return view('admin/categorylisting', ['categories' => $categories]);
})->middleware('auth','admin')->name('admin.categories');

///get users list
Route::get('/admin/manageusers', function () {
	$users = DB::table('users')->get();

    return view('admin/manageusers', ['users' => $users]);
})->middleware('auth','admin')->name('admin.manageusers');
///new users
Route::post('/admin/users/new', function (Request $request) {
	$request = $request->all();

	try {

    	$executed = DB::table('users')
	            ->insert([
	            	'name' => $request['name'], 
	            	'email' => $request['email'],
	            	'usertype' => $request['usertype'],
                    'password' => $request['password'],
                    'created_at' => date('Y-m-d H:i:s'),
	            	]);
	    return Response::json(array(
            'error' => false,
            'status' => 'success',
            'status_code' => 200
        ));
	} catch(\Exception $e){
	    return Response::json(array(
            'error' => true,
            'status' => 'failed',
            'error_message' => $e->getMessage(),
            'status_code' => 200
        ));
	}
})->middleware('auth','admin')->name('admin.users.new');



//facebook login
Route::get('login/facebook', 'Auth\LoginController@redirectToProvider')
	->name('login.fb');
Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback');

//trending user.trending
 
Route::get('/trending-quizzes', function () {
    $quizzes1=DB::Table('quiz')    
    
   ->select('id','name', 'slug', 'category_id','image','view_count')  
    ->orderBy('view_count','DESC')
    ->take(4)    
	->get();
    return view('user/userHome',['quizzes1'=>$quizzes1]);
})->name('user.trending');

///update users
Route::post('/admin/users/save', function (Request $request) {
	$request = $request->all();

	try {

    	$executed = DB::table('users')
	            ->where('id', $request['id'])
	            ->update([
	            	'name' => $request['name'], 
	            	'email' => $request['email'],
                    'password' => $request['password'],
	            	'usertype' => $request['usertype']
	            	]);
	    return Response::json(array(
            'error' => false,
            'status' => 'success',
            'status_code' => 200
        ));
	} catch(\Exception $e){
	    return Response::json(array(
            'error' => true,
            'status' => 'failed',
            'error_message' => $e->getMessage(),
            'status_code' => 200
        ));
	}
})->middleware('auth')->name('admin.quiz.save');

Route::post('/admin/question/save', function (Request $request) {
	$request = $request->all();

	try {

    	$executed = DB::table('question')
	            ->where('id', $request['id'])
	            ->update([
	            	'title' => $request['title'], 
	            	'option1' => $request['option1'],
	            	'option2' => $request['option2'],
	            	'option3' => $request['option3'],
	            	'option4' => $request['option4'],
	            	'status' => $request['status'],
	            	'correct_answer' => $request['correct_answer']
	            	]);
	    return Response::json(array(
            'error' => false,
            'status' => 'success',
            'status_code' => 200
        ));
	} catch(\Exception $e){
	    return Response::json(array(
            'error' => true,
            'status' => 'failed',
            'status_code' => 200
        ));
	}
})->middleware('auth','admin')->name('admin.question.save');

Route::post('/admin/quiz/save', function (Request $request) {
	$request = $request->all();

	try {

    	$executed = DB::table('quiz')
	            ->where('id', $request['id'])
	            ->update([
	            	'name' => $request['name'], 
	            	'description' => $request['description'],
                    'category_id' => $request['category'],
	            	'status' => $request['status']
	            	]);
	    return Response::json(array(
            'error' => false,
            'status' => 'success',
            'status_code' => 200
        ));
	} catch(\Exception $e){
	    return Response::json(array(
            'error' => true,
            'status' => 'failed',
            'error_message' => $e->getMessage(),
            'status_code' => 200
        ));
	}
})->middleware('auth')->name('admin.quiz.save');

Route::post('/admin/category/save', function (Request $request) {
	$request = $request->all();

	try {

    	$executed = DB::table('category')
	            ->where('id', $request['id'])
	            ->update([
	            	'name' => $request['name'], 
	            	'description' => $request['description'],
	            	'status' => $request['status']
	            	]);
	    return Response::json(array(
            'error' => false,
            'status' => 'success',
            'status_code' => 200
        ));
	} catch(\Exception $e){
	    return Response::json(array(
            'error' => true,
            'status' => 'failed',
            'error_message' => $e->getMessage(),
            'status_code' => 200
        ));
	}
})->middleware('auth')->name('admin.category.save');

//Route::get('/admin/csvquestion/new', 'QuestionController@importquestions')->name('import-questions');

Route::post('/admin/csvquestion/new','QuestionController@handleimportquestions')->name('bulk-import-questions');

Route::get('/admin/quizzes/{id}/questions', function ($id) {
    
	///incrementing view
DB::table('quiz')->where('id', $id)->increment('view_count');
	 
       
	$questions = DB::table('question')->where('quiz_id', $id)->get();
	$quiz = DB::table('quiz')->where('id', $id)->first();
	     
    return view('admin/questionlisting', ['quiz' => $quiz, 'questions' => $questions]);
})->middleware('auth','admin')->name('admin.questions');

//user get question

Route::get('/quiz/{slug}_{id}', function ($slug,$id) {
    
	///incrementing view
    DB::table('quiz')->where('id', $id)->increment('view_count');
	//echo '<pre>';
    $questions = DB::table('question')->where('quiz_id', $id)
        ->leftjoin('quiz', 'question.quiz_id', '=', 'quiz.id')
        ->select('question.*', 'quiz.QuestionCount As QuestionCount','quiz.category_id as category_id','quiz.score_count as score_count','quiz.name as quiz_name','quiz.image as quiz_image','quiz.slug as quiz_slug','quiz.description as quiz_description')
        ->take(10)
        ->orderByRaw("RAND()")
        ->get();	
	
	/*$relevant_quiz=DB::table('quiz')->where('category_id', $category_id)->take(10)->get();*/
    $quiz = DB::table('quiz')->where('id', $id)->first();
	

    
    //print_r($questions);
    //die();

    return view('user/UserQuestion', ['quiz' => $quiz, 'questions' => $questions]);
})->middleware('auth')->name('user.questions');

//user get Result

Route::get('/user/quizzes/{id}/result', function ($id) {
    
	///incrementing view
DB::table('quiz')->where('id', $id)->increment('view_count');
	 
	$questions = DB::table('question')->where('quiz_id', $id)
		->leftjoin('quiz', 'question.quiz_id', '=', 'quiz.id')
          ->select('question.*', 'quiz.QuestionCount As QuestionCount','quiz.category_id as category_id','quiz.score_count as score_count')
		->take(1)
		->orderByRaw("RAND()")
		->get();
		
	$quiz = DB::table('quiz')->where('id', $id)->first();
	

    
    return view('user/userResult', ['quiz' => $quiz, 'questions' => $questions]);
})->middleware('auth')->name('user.result');

/// user question post
//Route::post('/admin/csvquestion/new','QuestionController@handleimportquestions')->name('bulk-import-questions');
Route::post('/user/question/new','QuestionController@handleResults')  
		   ->middleware('auth')->name('user.question.new');
	
Auth::routes();


Route::get('/home', 'HomeController@index');

Route::post('/admin/question/new', function (Request $request) {
	$request = $request->all();

	try{
        
    	$executed = DB::table('question')
	            ->insert([
	            	'title' => $request['title'], 
	            	'option1' => $request['option1'],
	            	'option2' => $request['option2'],
	            	'option3' => $request['option3'],
	            	'option4' => $request['option4'],
	            	'status' => $request['status'],
                    'quiz_id' => $request['quiz_id'],
	            	'correct_answer' => $request['correct_answer'],
                    'created_at' => date('Y-m-d H:i:s')
	            	]);
	    return Response::json(array(
            'error' => false,
            'status' => 'success',
            'status_code' => 200
        ));
	} catch(\Exception $e){
	    return Response::json(array(
            'error' => true,
            'status' => 'failed',
            'status_code' => 200
        ));
	}
})->middleware('auth')->name('admin.question.new');






Route::post('/admin/quiz/new', function (Request $request) {
	$request = $request->all();

    
	try {
          

    	$executed = DB::table('quiz')
	            ->insert([
	            	'name' => $request['name'], 
	            	'description' => $request['description'],
                    'category_id' => $request['category'],
	            	'status' => $request['status'],
                    'created_at' => date('Y-m-d H:i:s'),
                    'user_id' => Auth::user()->id
	            	]);
	    return Response::json(array(
            'error' => false,
            'status' => 'success',
            'status_code' => 200
        ));
	} catch(\Exception $e){
	    return Response::json(array(
            'error' => true,
            'status' => 'failed',
            'error_message' => $e->getMessage(),
            'status_code' => 200
        ));
	}
})->middleware('auth')->name('admin.quiz.new');

//adding new featured quiz
Route::post('/admin/featured/new', function (Request $request) {
	$request = $request->all();

    
	try {
          

    	$executed = DB::table('featuredquizzes')
	            ->insert([          	
                    
	            	'Quiz_id'=>$request['quiz'],
                    'created_at' => date('Y-m-d H:i:s'),
                    'uid' => Auth::user()->id
	            	]);
	    return Response::json(array(
            'error' => false,
            'status' => 'success',
            'status_code' => 200
        ));
	}
    
    catch(\Exception $e){
	    return Response::json(array(
            'error' => true,
            'status' => 'failed',
            'error_message' => $e->getMessage(),
            'status_code' => 200
        ));
	}
})->middleware('auth')->name('admin.featured.new');

//updating existing featured quiz
Route::post('/admin/featured/save', function (Request $request) {
	$request = $request->all();
     
	try {
          
        
    	$executed = DB::table('featuredquizzes')
			->where('id', $request['id'])
	            ->update([
	            	'Quiz_id' => $request['quiz'],
					'uid' => Auth::user()->id,
				]);
		
	    return Response::json(array(
            'error' => false,
            'status' => 'success',
            'status_code' => 200
        ));
	}
    
    catch(\Exception $e){
	    return Response::json(array(
            'error' => true,
            'status' => 'failed',
            'error_message' => $e->getMessage(),
            'status_code' => 200
        ));
	}
})->middleware('auth')->name('admin.featured.new');

Route::post('/admin/category/new', function (Request $request) {
	$request = $request->all();

	try {

    	$executed = DB::table('category')
	            ->insert([
	            	'name' => $request['name'], 
	            	'description' => $request['description'],
	            	'status' => $request['status'],
                    'user_id' => Auth::user()->id,
                    'created_at' => date('Y-m-d H:i:s'),
	            	]);
	    return Response::json(array(
            'error' => false,
            'status' => 'success',
            'status_code' => 200
        ));
	} catch(\Exception $e){
	    return Response::json(array(
            'error' => true,
            'status' => 'failed',
            'error_message' => $e->getMessage(),
            'status_code' => 200
        ));
	}
})->middleware('auth')->name('admin.category.new');
Auth::routes();

Route::get('/home','HomeController@index');
//////delete 
//delete user
Route::get('/admin/users/delete/{id}', function ($id){    
	DB::table('users')->where('id', '=', $id)->delete();
return redirect()->route('admin.manageusers'); 

})->middleware('auth','admin')->name('delete.user');

//delete quiz
Route::get('/admin/quiz/delete/{id}', function ($id){    
	DB::table('quiz')->where('id', '=', $id)->delete();
return redirect()->route('admin.quizzes'); 

})->middleware('auth','admin')->name('delete.quiz');

//delete featured
Route::get('/admin/featured/delete/{id}', function ($id){    
	DB::table('featuredquizzes')->where('id', '=', $id)->delete();
return redirect()->route('admin.featured'); 

})->middleware('auth','admin')->name('featured.delete');

//delete category
Route::get('/admin/categories/delete/{id}', function ($id) {
	DB::table('category')->where('id', '=', $id)->delete();
return redirect()->route('admin.categories'); 
})->middleware('auth','admin')->name('delete.category');
