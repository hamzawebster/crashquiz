<?php

use Illuminate\Http\Request;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/v1/quizzes', function() {

  //$quizzes = App\Quiz::find($id, array('id', 'name', 'description'));
  $quizzes = App\Quiz::where('status', 'active')->take(20)->get();

  return Response::json(array(
            'error' => false,
            'quizzes' => $quizzes,
            'status_code' => 200
        ));
});

Route::get('/v1/quizzes/{id}', function($id) {

  $quiz = DB::table('quiz')->where('id', $id)->first();
  $quiz_questions = DB::table('question')->where('status', 'active')->where('quiz_id', $id)->inRandomOrder()->take(10)->get();

  $quiz->total_questions = count($quiz_questions);

  return Response::json(array(
            'error' => false,
            'quiz' => $quiz,
            'questions' => $quiz_questions,
            'status_code' => 200
        ));
});


/*Route::group(['middleware' => 'cors'], function(Router $router){
    $router->get('api', 'ApiController@index');
});*/


Route::get('/v1/quizzes_name/{s_param?}', function($s_param = null) {
if ($s_param == null) {
    $quizzes = App\Quiz::where('status', 'active')->take(20)->get();
} else {
    //$quizzes = App\Quiz::find($id, array('id', 'name', 'description'));
    $quizzes = App\Quiz::where('name', 'LIKE', '%'. $s_param . '%')->get();
}
return Response::json(array(
            'error' => false,
            'quizzes' => $quizzes,
            'status_code' => 200
        ));
});




/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/quizzes', function() {
  //$quizzes = App\Quiz::find($id, array('id', 'name', 'description'));       
    
   $quizzes = App\Quiz::select( 'Quiz.id','Quiz.name as QuizName','Quiz.description','Quiz.average_playtime','Quiz.status','image','Quiz.thumbnail','Quiz.created_at','Quiz.updated_at','category_id','Quiz.user_id','category.name as categoryName',DB::raw('Quiz.created_at >= DATE_SUB(CURDATE(),INTERVAL 10 day) as isNew'))
                            
                            
                          
                        
       ->join('category', 'Quiz.category_id', '=', 'category.id')
       
       ->where('Quiz.status', 'active')->take(20)->get();
    
        
        
       return Response::json(array(
            'error' => false,
            
            'quizzes' => $quizzes,
            'status_code' => 200
        ));
       
  
});


Route::get('/quizzes/{id}', function($id) {

  $quiz = DB::table('quiz')->where('id', $id)->first();
  $quiz_questions = DB::table('question')->where('status', 'active')->where('quiz_id', $id)->inRandomOrder()->take(10)->get();

  $quiz->total_questions = count($quiz_questions);

  return Response::json(array(
            'error' => false,
            'quiz' => $quiz,
            'questions' => $quiz_questions,
            'status_code' => 200
        ));
});


/*Route::group(['middleware' => 'cors'], function(Router $router){
    $router->get('api', 'ApiController@index');
});*/


Route::get('/quizzes_name/{s_param?}', function($s_param = null) {
if ($s_param == null) {
    $quizzes = App\Quiz::where('status', 'active')->take(20)->get();
} else {
    //$quizzes = App\Quiz::find($id, array('id', 'name', 'description'));
    $quizzes = App\Quiz::where('name', 'LIKE', '%'. $s_param . '%')->get();
}
return Response::json(array(
            'error' => false,
            'quizzes' => $quizzes,
            'status_code' => 200
        ));
});

Route::get('/v1/categories', function() {

  //$quizzes = App\Quiz::find($id, array('id', 'name', 'description'));
  $categories = App\Category::where('status', 'active')->take(20)->get();

  return Response::json(array(
            'error' => false,
            'categories' => $categories,
            'status_code' => 200
        ));
});

Route::get('/v1/quizzes/category/{category_id}', function($category_id) {
    
  try {

    	$quizzes = App\Quiz::where('status', 'active')->where('category_id', $category_id)->take(20)->get();
	    return Response::json(array(
            'error' => false,
            'quizzes' => $quizzes,
            'status_code' => 200
        ));
	} catch(\Exception $e){
	    return Response::json(array(
            'error' => true,
            'status' => 'failed',
            'error_message' => $e->getMessage(),
            'status_code' => 200
        ));
	}
});


Route::post('/admin/featured/new', function (Request $request) {
	$request = $request->all();

    
	try {
          

    	$executed = DB::table('featuredquiz')
	            ->insert([          	
                    
	            	'Quiz_id'=>$request['quiz'],
                    'created_at' => date('Y-m-d H:i:s'),
                    'uid' => 2
	            	]);
	    return Response::json(array(
            'error' => false,
            'status' => 'success',
            'status_code' => 200
        ));
	}
    
    catch(\Exception $e){
	    return Response::json(array(
            'error' => true,
            'status' => 'failed',
            'error_message' => $e->getMessage(),
            'status_code' => 200
        ));
	}
});

